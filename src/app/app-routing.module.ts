import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FruitcartComponent } from './fruitcart/fruitcart.component';

const routes: Routes = [
  {
  path:'fruitcart',
   component: FruitcartComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
