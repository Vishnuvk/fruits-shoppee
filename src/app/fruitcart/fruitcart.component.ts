import { Component, OnInit } from '@angular/core';
import FruitData from '../fruit.json';
interface FruitCart  {  
  p_name: any;
  p_id:any;
  p_cost:any;
  p_availability:any;
  p_details:any;
  p_category:any;
  p_img: any;
 
}  

@Component({
  selector: 'app-fruitcart',
  templateUrl: './fruitcart.component.html',
  styleUrls: ['./fruitcart.component.css']
})
export class FruitcartComponent implements OnInit {
  name = 'Angular';  
  fruits: FruitCart[] = FruitData;  
  qty: any = 1; 
  fruit:any = [];
  category: any;
  products:any;

  constructor() { }

  ngOnInit(): void {
  this.categoryData('All');}
  
  inc(value: any) {
    if (value) {
      this.qty = this.qty + 1;
    } else if (this.qty > 1 && !value) {
      this.qty = this.qty - 1;
    } else {
    }
  }
  categoryData(category: any) {
    this.fruit = FruitData.filter(object => {
      if (category == 'All') {
        return object['p_name'];
      } else {
        return object['p_category'] == category;
      }
    }); 
  }
  product(products:any){
   
  }
}
